#include <iostream>
#include <memory>
#include <string>
#include <map>
#include <iterator>

void getEntry(std::map<std::string,std::string>& people);
void search(std::map<std::string,std::string>& people);

int main(int argc, char const *argv[])
{
  std::map<std::string,std::string> people;
  enum mode {
    LIST, ENTER, SEARCH, EXIT
  };

  std::cout << "Enter names and adresses." << std::endl;
  std::cout << "[0] - print map" << std::endl;
  std::cout << "[1] - make entry" << std::endl;
  std::cout << "[2] - search names" << std::endl;
  std::cout << "[3] - back/exit" << std::endl;

  while (true) {
    std::string choice;
    getline(std::cin, choice);

    switch(atoi(choice.c_str())) {
      case LIST:
        for (auto i : people)
          std::cout << i.first << "\t" << i.second << std::endl;
        break;
      case ENTER:
        getEntry(people);
        break;
      case SEARCH:
        search(people);
        break;
      case EXIT:
        return 0;
      default:
        return 0;
    }
  }
  return 0;
}

void getEntry(std::map<std::string,std::string>& people) {
  while (true) {
    /**
     * Get user input
     */
    std::cout << "Name: ";
    std::string name;
    getline(std::cin, name);
    if (!std::cin) break;
    if (name == "3") break;
    std::cout << "City: ";
    std::string city;
    getline(std::cin, city);

    /**
     * Insert returns a std::pair. The pair::second element is true if the
     * element was inserted and false if the key already existed.
     */
    auto j = people.insert(std::make_pair(name,city));
    if (!j.second)
      std::cout << name <<
          " war schon eingetragen. Bitte anderen Namen verwenden." << std::endl;
  }
}

void search(std::map<std::string,std::string>& people) {
  while (true) {
    /**
     * Get user input
     */
    std::cout << "Suche: ";
    std::string search;
    getline(std::cin, search);
    if (!std::cin) break;
    if (search == "3") break;

    /**
     * use std::map::find() to return the value corresponding to the given key
     */
    auto res = people.find(search);
    if (res != people.end()) {
      std::cout << people.find(search)->second << std::endl;
    } else {
      std::cout << search << " wurde nicht gefunden." << std::endl;
    }
  }
}
