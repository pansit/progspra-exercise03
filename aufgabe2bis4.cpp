#include <cstdlib>    // std::rand()
#include <vector>     // std::vector<>
#include <list>       // std::list<>
#include <iostream>   // std::cout
#include <iterator>   // std::ostream_iterator<>
#include <algorithm>  // std::reverse, std::generate
#include <map>        // std::map
#include <set>        // std::set
#include <time.h>

int main(int argc, char const *argv[])
{
  /**
   * The pseudo-random number generator is initialized using the argument passed
   * as seed.
   */
  srand(time(NULL));

  /**
   * Aufgabe 3.2 a
   * Instanziieren Sie eine std::list mit unsigned int und füllen Sie diese mit
   * 100 Zufallszahlen zwischen 0 und 50.
   */
  std::list<unsigned int> l0(100);
  for (std::list<unsigned int>::iterator i = l0.begin(); i != l0.end(); ++i)
    *i = std::rand() % 50;

  /**
   * Aufgabe 3.2 b
   * Erzeugen Sie einen std::vector und kopieren Sie mit std::copy die Elemente
   * der Liste in den std::vector.
   *
   * std::vector<unsigned int> v0(l0.begin(), l0.end()); would be even shorter
   * but still pretty clear.
   */
  std::vector<unsigned int> v0(l0.size());
  std::copy(l0.begin(), l0.end(), v0.begin());

  /**
   * Aufgabe 3.3
   * Bestimmen Sie, wieviele unterschiedliche Zahlen in der std::list aus
   * Aufgabe 3.2 sind und geben Sie die Zahlen von 0 bis 50 aus, die nicht in
   * der Liste sind.
   */

  /**
   * Aufgabe 3.3 a
   * The size of the set will be equal to the amount of different numbers in l0.
   */
  std::set<unsigned int> diffSet;
  for (auto i : l0) diffSet.insert(i);
  std::cout << "Unterschiedliche Zahlen in der std::list: " << diffSet.size()
            << std::endl;

  /**
   * Aufgabe 3.3 b
   * The if-clause checks the set for any given 'i'. If set::find() reaches
   * std::end() 'i' is not in the set and is written to the ostream object.
   */
  std::cout << "Zahlen von 0 bis 50, nicht in der std::list: ";
  for (unsigned int i = 0; i <= 50; ++i)
    if (diffSet.count(i) == 0) std::cout << i << " ";
  std::cout << "\n";

  /**
   * Aufgabe 3.4
   * Ermitteln Sie die Häufigkeit jeder Zahl in der std::list aus Aufgbae 3.2.
   * Erklären Sie, warum sich std::map für dieses Problem anbietet.
   */
  std::map<unsigned int, unsigned int> strichListe;
  for (unsigned int i = 0; i <= 50; ++i) strichListe[i] = 0;
  for (auto i : l0) strichListe[i]++;

  /**
   * Geben Sie die Häufigkeit aller Zahlen in der Form "Zahl: Häufigkeit" auf
   * der Konsole aus.
   */
  for (auto i : strichListe)
    std::cout << i.first << ":\t" << i.second << std::endl;
}
