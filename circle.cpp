#include "circle.hpp"
#include <cmath>

Circle::Circle()
  : radius_{0.0}
{}

Circle::Circle(double const radius)
  : radius_{radius}
{}

Circle::~Circle(){}

double Circle::radius() const {
  return radius_;
}

double Circle::circumference() const {
  return 2 * M_PI * radius_;
}

bool operator< (Circle const& p1, Circle const& p2) {
  return p1.radius() < p2.radius();
}
bool operator> (Circle const& p1, Circle const& p2) {
  return p1.radius() > p2.radius();
}
bool operator== (Circle const& p1, Circle const& p2) {
  return p1.radius() == p2.radius();
}
std::ostream& operator<<(std::ostream& os, Circle const& c) {
  os << "Radius: " << c.radius();
  return os;
}