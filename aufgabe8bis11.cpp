#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "circle.hpp"
#include "templates.hpp"
#include <cmath>
#include <list>
#include <iterator>
#include <algorithm>

TEST_CASE("filter", "[aufgabe5]")
{
  /**
   * Aufgabe 3.5
   * Füllen Sie einen std::vector von unsigned int mit 100 Zufallszahlen
   * zwischen 0 und 50, Entfernen Sie alle ungeraden Zahlen. Verwenden Sie dafür
   * einen passenden STL-Algorithmus.
   * Testen Sie danach mit std::all_of aus <algorithm>, ob alle Elemente im
   * vector gerade sind. Schreiben Sie dafür eine Hilfsfunktion is_even.
   */
  std::vector<unsigned int> v0(100);
  for (std::vector<unsigned int>::iterator it = v0.begin(); it != v0.end();
        ++it)
    *it = std::rand() % 50 + 1;

  filterfy(v0,is_odd);

  REQUIRE(std::all_of(v0.begin(), v0.end(), is_even));
}

TEST_CASE("sort_circles0", "[aufgabe8]")
{
  /**
   * Aufgabe 3.8
   * Objekt der Klasse Circle sollen in einem STL-Container gesperichert und der
   * Radiusgröße nach sortiert werden. Um Objekte in einem Container sortieren
   * zu können, müssen Sie verleichbar sein. Überladen Sie die Operatoren
   * operator<, operator> und operator== für Objekte vom Typ Circle, füllen Sie
   * einen Container mit Objekten vom Typ Circle und sortieren Sie diesen.
   */
  std::list<Circle> l0(50);
  for (std::list<Circle>::iterator it = l0.begin(); it != l0.end(); ++it)
    *it = Circle{(double)(std::rand()%10)};

  l0.sort();
  REQUIRE(std::is_sorted(l0.begin(), l0.end()));
}

TEST_CASE("sort_circles1", "[aufgabe8]")
{
  std::list<Circle> l0(50);
  std::generate(std::begin(l0), std::end(l0), circleGenerator);

  l0.sort();
  REQUIRE(std::is_sorted(l0.begin(), l0.end()));
}

TEST_CASE("swap1", "[aufgabe9]")
{
  /**
   * Aufgabe 3.9
   */
  int a = 5;
  int b = 10;
  swap(a,b);

  REQUIRE(a == 10);
  REQUIRE(b == 5);
}

TEST_CASE("swap2", "[aufgabe9]")
{
  /**
   * Aufgabe 3.9
   */
  Circle a{34};
  Circle b{1};
  swap(a,b);

  REQUIRE(a == 1);
  REQUIRE(b == 34);
}

TEST_CASE("concatenate0", "[aufgabe10]")
{
  /**
   * Aufgabe 3.10
   */
  std::vector<int> v{1,2,3,4,5};
  std::vector<int> k{6,7,8,9,10};

  auto m = concatenate(v,k);
  REQUIRE(m.size() == 10);
  REQUIRE(m[7] == 8);
}

TEST_CASE("concatenate1", "[aufgabe10]")
{
  /**
   * Aufgabe 3.10
   */
  std::list<int> v{1,2,3,4,5};
  std::list<int> k{6,7,8,9,10};

  auto m = concatenate(v,k);
  REQUIRE(m.size() == 10);

  auto it = m.begin();
  std::advance(it, 6);
  REQUIRE(*it == 7);
}

TEST_CASE("concatenate2", "[aufgabe10]")
{
  /**
   * Aufgabe 3.10
   */
  std::vector<Circle> v(10);
  std::generate(std::begin(v), std::end(v), circleGenerator);
  std::vector<Circle> k(10);
  std::generate(std::begin(k), std::end(k), circleGenerator);
  REQUIRE(concatenate(v,k).size() == 20);
}

TEST_CASE("filter0", "[filter]")
{
  /**
   * Aufgabe 3.11
   */
  std::vector<int> v{1,2,3,4,5,6};
  std::vector<int> alleven = filter(v, is_odd);

  REQUIRE(std::all_of(alleven.begin(), alleven.end(), is_even));
}

int main(int argc, char* argv[])
{
  return Catch::Session().run(argc, argv);
}