#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "templates.hpp"
#include <algorithm>

TEST_CASE("describe_factorial", "[aufgabe3]")
{
  /**
   * Aufgabe 3.5
   * Füllen Sie einen std::vector von unsigned int mit 100 Zufallszahlen
   * zwischen 0 und 50, Entfernen Sie alle ungeraden Zahlen. Verwenden Sie dafür
   * einen passenden STL-Algorithmus.
   * Testen Sie danach mit std::all_of aus <algorithm>, ob alle Elemente im
   * vector gerade sind. Schreiben Sie dafür eine Hilfsfunktion is_even.
   */
  std::vector<unsigned int> v0(100);
  for (auto i : v0) i = std::rand() % 50;

  v0.erase(std::remove_if(v0.begin(), v0.end(), is_odd), v0.end());

  REQUIRE(std::all_of(v0.begin(), v0.end(), is_even));
}

int main(int argc, char* argv[])
{
  return Catch::Session().run(argc, argv);
}