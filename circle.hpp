#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include <iostream>

class Circle
{
public:
  Circle();
  Circle(double const radius);
  ~Circle();

  double radius() const;
  double circumference() const;

  friend bool operator<(Circle const& c1, Circle const& c2);
  friend bool operator>(Circle const& c1, Circle const& c2);
  friend bool operator==(Circle const& c1, Circle const& c2);
  friend std::ostream& operator<<(std::ostream& os, Circle const& c);

private:
  double radius_;

  // Hälfte der Segmente mit denen der Kreis gezeichnet wird.
  unsigned char segments_ = 60;
};

#endif // CIRCLE_HPP