#Aufgabensammlung 3

##Programmiersprachen SoSe 2015
Use `git clone <rep>` and then `mkdir build && cd build && cmake ..`
This repo uses the testing environment [Catch](https://raw.githubusercontent.com/philsquared/Catch/develop/single_include/catch.hpp).

##sequentielle vs. assoziative Container
###sequentiell
Sequentielle Container arrangieren ihre Elemente nacheinander in einer festen Reihenfolge. Aus diesem Grund ist die Komplexität von Suche und Zugriff `O(N)` und am Anfang oder Ende des Containers `O(1)`.
* Ein `vector<T>` (dynamisches Feld) erlaubt den schnellsten indizierten Zugriff und kann leicht am Ende wachsen oder schrumpfen
* Eine `deque<T>` (double-ended queue) kann nach beiden Seiten wachsen oder schrumpfen
* Eine `list<T>` (doppelt verkettete Liste) lässt sich durch das Umhängen von Zeigern gut umordnen, ohne Elemente kopieren oder zuweisen zu müssen, bietet jedoch keinen indizierten Zugriff

```cpp
std::vector<T>      [0|1|2|3|4] <-->
std::deque<T>  <--> [0|1|2|3|4] <-->
std::list<T>   <--> [0]=[1]=[2]=[3]=[4] <-->
```

###assoziativ
Da assoziative Container meist als balancierte Binärbäume implementiert sind, haben Operationen zumeist `O(logN)` (logarithmisches) Laufzeitverhalten.
* Tabellen `map<Key,Value,Compare>` verwalten Schlüssel-Wert-Paare `pair<Key,Value>`. Jedem Schlüssel ist ein anderer Wert zugeordnet.
* Die `multimap<Key,Value,Compare>` kann mehrere Werte dem gleichen Schlüssel zuordnen.
* Mengen `set<T,Compare>` können als degenerierte assoziative Felder betrachtet werden (nur Schlüssel ohne zugeordneten Wert).
* In `multiset<T,Compare>` dürfen gleiche Werte auch mehrfach vorkommen.
```cpp
std::map<std::string, int>
Meier   6135
Meyer   9999
Schulze 5180

std::multimap<std::string, int>
Meier   6135
Meier   6134
Meyer   9999
Schulze 5180

std::set<int>
{ 1 2 3 4 5 }

std::multiset<int, std::greater<int>>
{ 5 5 4 3 3 }
```

###Anwendung
* Speichern der Punkte eines Polygons - **vector (sequentiell)**: Einzelne Punkte eines Polygons werden in der Regel nacheinander abgearbeitet und müssen nicht über einen Key direkt erreichbar sein.

* Zuordnung von Farbnamen und entsprechenden RGB-Werten - **map (assoziativ)**: Perfektes Beispiel für den Anwendungsfall einer std::map da die jeweiligen RGB-Werte direkt mit Namen gesucht werden können.

* FIFO-Warteschlange von Druckaufträgen - **deque (sequentiell)**: Ermöglicht effizientes Einfügen und Löschen am Anfang und am Ende des Containers. Wird oftmals in Verbindung mit dem queue-Adapter verwendet.

###Häufige Fehler
Bei folgendem Programmsegment kann es zu Problemen kommen:
```cpp
std::map<string,int> matrikelnummern;
// Hinzufügen von vielen Studenten
matrikelnummern["Max Mustermann"] = 123456;
matrikelnummern["Erika Mustermann"] = 23523;
// ...
exmatrikulation(matrikelnummer["Fred Fuchs"]);
```
* Wenn beim Einfügen von Elementen der `operator[]` verwendet wird, kann es passieren, dass vorhandene Elemente einen anderen Wert bekommen wenn der Schlüssel mehrfach benutzt wird. Besser man verwenden [`std::map::insert()`](http://en.cppreference.com/w/cpp/container/map/insert). Hier wird ein vorhandenes Schlüssel/Wert-Paar nicht geändert. Außerdem gibt die Funktion ein `std::pair` zurück. `pair::first` ist ein Iterator der auf neu eingefügte oder vorhandene Element zeigt. `pair::second` ist ein Bool der auf `true` gesetzt ist wenn ein Element eingefügt wurde und `false` wenn der Schlüssel schon vorhanden war.
* Wenn `exmatrikulation()` aufgerufen wird kann es passieren, dass die Funktion ein Element als Parameter übergeben bekommt das nicht existiert. `operator[]` wird in diesem Fall ein neues Element erzeugen, indem für `pair::second` der Standardkonstruktor aufgerufen wird. Um sicher zu gehen, dass ein bestimmtes Element auch existiert, kann die Funktion [`std::map::find()`](http://en.cppreference.com/w/cpp/container/map/find) verwendet werden.

##Templates
###Template-Funktionen
Viele Algorithmen wie `swap<T>`, `max<T>` oder `min<T>` funktionieren für unterschiedliche Datentypen auf gleiche Art und Weise. Mit der normalen Implementierung müsste man eine Funktion allerdings für jeden Datentyp überladen damit die Methode für ihn anwendbar wird.

Besser ist es wenn man den Algorithmus allgemein beschreibt und den Datentyp, auf den er angewendet werden soll, außen vor lässt:
```cpp
/**
 * Statt typename kann auch class geschrieben werden.
 * Die Parameterübergabe erfolgt per Value.
 *
 * Diese Funktion kann nun für alle Datentypen verwendet werden, für die der
 * operator< überladen ist.
 */
template<typename T>
T max(T a, T b) {
  return (a < b) ? b : a;
}
```
Bei der Übersetzung entscheidet der Compiler anhand des Funktionsaufrufs für welchen Typ die Funktion generiert werden muss. Dabei wird der Platzhalter `T` an jeder stelle durch den jeweiligen Typ ersetzt.

###Template-Klassen
Template-Klassen sind verallgemeinerte Klassen, die mit einer Menge von Datentypen funktionieren können. Die Container der STL sind nur deshalb so vielseitig anwendbar, weil sie alle als Template-Klassen implementiert sind. So kann ein std::vector mit einem beliebigen Datentyp als Inhalt erzeugt werden.
```cpp
/**
 * Beispiel: range-constructor eines std::vector
 */
template<class InputIterator>
vector (InputIterator first, InputIterator last,
        const allocator_type& alloc = allocator_type());
```

##Glossar
* **Freispeicher/Freestore:** Der Speicher, der von jedem C++-Programm genutzt wird, ist in vier Bereiche aufgeteilt.
    * Code
    * Globale Variablen
    * Heap oder Freestore
    * Call Stack

    Der Freestore ist der Bereich wo dynamisch erzeugte Variablen Platz finden. Eigenschaften des Freestore sind:

    * Allokierter Speicher bleibt solange belegt, bis er explizit wieder frei gegeben wir
    * Auf dynamisch erzeugte Objekte wird immer per Zeiger zugegriffen
    * Wegen seiner Größe sollten große Datenstrukturen und Container hier gespeichert werden

    Weitere Quellen: [The Stack and the Heap – LearnCpp.Com](http://www.learncpp.com/cpp-tutorial/79-the-stack-and-the-heap/), [Stack vs. Heap – Paul Gribble (2013)](http://gribblelab.org/CBootcamp/7_Memory_Stack_vs_Heap.html)

* **Template:** Mit Templates können Algorithmen allgemein beschrieben werden, ohne sie von Datentypen abhängig zu machen. Dies wird auch generische Programmierung genannt.

    Weitere Quellen: [Generische Programmierung (Templates) – Arnold Willemer (2005)](http://www.willemer.de/informatik/cpp/cpptempl.htm)

* **STL (Standard Template Library):** Hochflexible Sammlung von Template-Klassen.

* **Sequentielle Container:** Sequentielle Container organisieren ihre Elemente in einer vorgegebener Reihenfolge und behalten diese bei solange sie nicht explizit geändert werden (z.B. `std::reverse()`).

* **Assoziativer Container:** Assoziative Container verbinden die gespeicherten Werte mit Schlüsseln, nach denen die Elemente sortiert gehalten werden.

* **Iteratoren:** Iteratoren sind Objekte die sich ähnlich wie Pointer verhalten. Allerdings haben sie die Fähigkeit über einen Bereich (z.B. `std::list<T>`) zu iterieren. Dafür nutzen sie Operatoren wie increment, decrement, dereference etc.

* **STL-Algorithmen:** Die STL-Algorithmen bestehen aus einer Reihe nützlicher Funktionen, die auf die Template-Klassen der STL angewendet werden können. Algorithmen arbeiten mit Iteratoren auf Containern.

    Weitere Quellen: [Algorithms library – cppreference.com](http://en.cppreference.com/w/cpp/algorithm)

* **Komplexität std::vector:**
    * Einfügen und löschen am Ende in `O(1)` **- effizient**
    * Einfügen und löschen überall in `O(N)`
    * Suchen in `O(N)`

* **Komplexität std::list:**
    * Einfügen und löschen überall in `O(1)` **- effizient** falls Iterator bekannt, sonst Suche erforderlich
    * Suchen in `O(N)`

* **Komplexität std::set:**
    * Einfügen und Löschen überall in `O(logN)`
    * Suchen in `O(logN)`

* **Komplexität std::map:**
    * Einfügen und Löschen überall in `O(logN)`
    * Suchen in `O(logN)`
