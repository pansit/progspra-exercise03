#include <cstdlib>    // std::rand()
#include <vector>     // std::vector<>
#include <list>       // std::list<>
#include <iostream>   // std::cout
#include <iterator>   // std::ostream_iterator<>
#include <algorithm>  // std::reverse, std::generate

int main(int argc, char const *argv[])
{
  /**
   * Erzeugt einen Container des Typs std::vector mit dem Namen v0 und der
   * Länge 10.
   */
  std::vector<int> v0(10);

  /**
   * for-Schleife füllt std::vector mit Hilfe von Iterator i mit
   * pseudozufälligen Zahlen.
   */
  for (std::vector<int>::iterator i = v0.begin(); i != v0.end(); ++i) {
    *i = std::rand();
  }

  /**
   * std::copy kopiert den Inhalt von v0 per ostream_iterator in das ostream-
   * Objekt.
   */
  std::copy(std::begin(v0), std::end(v0),
            std::ostream_iterator<int>(std::cout, "\n"));

  /**
   * Eine neue Variable vom Typ std::list<int> wird angelegt und bekommt die
   * Länge von v0.
   */
  std::list<int> l0(v0.size());

  /**
   * std::copy kopiert den Inhalt von v0 nach l0.
   */
  std::copy(std::begin(v0), std::end(v0), std::begin(l0));

  /**
   * Eine neue Variable vom Typ std::list<int> wird angelegt indem über
   * Iteratoren der Inhalt von l0 kopiert wird.
   */
  std::list<int> l1(std::begin(l0), std::end(l0));

  /**
   * Die Reihenfolge der Elemente von l1 wird umgedreht.
   */
  std::reverse(std::begin(l1), std::end(l1));

  /**
   * Die Elemente von l1 werden in das ostream-Objekt geschrieben.
   */
  std::copy(std::begin(l1), std::end(l1),
            std::ostream_iterator<int>(std::cout, "\n"));

  /**
   * Die Elemente von l1 werden sortiert. Default: ASC
   */
  l1.sort();

  /**
   * Die Elemente von l1 werden in das ostream-Objekt geschrieben.
   */
  std::copy(l1.begin(), l1.end(),
            std::ostream_iterator<int>(std::cout, "\n"));

  /**
   * std::generate überschreibt die Elemente von v0 mit pseudozufälligen Zahlen.
   */
  std::generate(std::begin(v0), std::end(v0), std::rand);

  /**
   * Die Elemente von v0 werden in umgekehrter Reihenfolge (rbegin(), rend() !)
   * das ostream-Objekt geschrieben.
   */
  std::copy(v0.rbegin(), v0.rend(),
            std::ostream_iterator<int>(std::cout, "\n"));

  return 0;
}