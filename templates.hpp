#include "circle.hpp"

#include <algorithm>
#include <vector>
#include <list>
#include <cstdlib>

template<typename T>
void swap(T& a, T& b) {
  T temp = a;
  a = b;
  b = temp;
}

template<typename T>
T concatenate(T const& a, T const& b) {
  T temp(a.begin(), a.end());
  temp.insert(temp.end(), b.begin(), b.end());
  return temp;
}

template<typename T1, typename T2>
T1 filter(T1 c, T2 const& Predicate) {
  c.erase(std::remove_if(c.begin(), c.end(), Predicate), c.end());
  return c;
}

template<typename T1, typename T2>
void filterfy(T1& c, T2 const& Predicate) {
  c.erase(std::remove_if(c.begin(), c.end(), Predicate), c.end());
}

bool is_odd(int const& i) {
  return i % 2 == 1;
}

bool is_even(int const& i) {
  return i % 2 == 0;
}

Circle circleGenerator() {
  return Circle{(double)(std::rand()%50)};
}